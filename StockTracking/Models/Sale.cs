﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StockTracking.Models
{
    [Table("TblSale")]
    public class Sale
    {   
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SaleID { get; set; }

        public int SaleAmount { get; set; }

        public Nullable<int> ProductID { get; set; }

        public virtual Product Product { get; set; }
    }
}