﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StockTracking.Models
{
    [Table("TblProduct")]
    public class Product
    {   
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductID { get; set; }
        [StringLength(50)]
        public string ProductName { get; set; }
        [Required]
        public int ProductAmount { get; set; }
        
        public int CategoryID { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }


    }
}