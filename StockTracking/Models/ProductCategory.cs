﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StockTracking.Models
{
    [Table("TblProductCategory")]
    public class ProductCategory
    {   
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryID { get; set; }
        [StringLength(20)]
        public string CategoryName { get; set; }

        //public virtual Product Product { get; set; }
    }
}