﻿using StockTracking.Models;
using StockTracking.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StockTracking.Controllers
{
    public class HomeController : Controller
    {
       [HttpGet]
        public ActionResult HomePage()
        {
            DatabaseContext db = new DatabaseContext();
        

            HomePageViewModels model = new HomePageViewModels();

            model.productCategory = db.ProductCategory.ToList();
            model.Product = db.Product.ToList();
            model.Sale = db.Sale.ToList();





            return View(model);
        }

        //[HttpPost]

        //public ActionResult HomePage()

        //{


        //}
    }
}