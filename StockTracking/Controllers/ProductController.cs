﻿using System;
using StockTracking.ViewModels.Home;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StockTracking.Models;

namespace StockTracking.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult AddProduct()
        {   


            return View();
        }

        [HttpPost]
        public ActionResult AddProduct(Product product)
        {

            DatabaseContext db = new DatabaseContext();
            db.Product.Add(product);
            db.SaveChanges();
            


            return RedirectToAction("Home", "HomePage");

        }
    }
}