﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StockTracking.Models;
namespace StockTracking.ViewModels.Home
{
    public class HomePageViewModels
    {
        public List<ProductCategory> productCategory { get; set; }
        public List<Product> Product { get; set; }
        public List<Sale> Sale { get; set; }
    }
}